import json


def main():
    
    # load components data
    data = None
    with open('all_components.json') as file:
        data = json.load(file)
    
    with open('components.csv', 'w') as file:    
        
        # Motherboards
        for mb in data['Motherboards']:
            line = mb['name'] + ','+'Motherboard'
            for i in mb['interfaces']:
                line += ',hasInterface,' + i
            file.write(line)
            file.write('\n')
            
        # CPUs
        for socket in data['CPUs']:
            for component_class in data['CPUs'][socket]:
                for name in data['CPUs'][socket][component_class]:
                    line = name + ',' + component_class + ',hasInterface,' + socket
                    file.write(line)
                    file.write('\n')
                    
        
        # GPUs
        for pcie_version in data['GPUs']:
            for name in data['GPUs'][pcie_version]:
                line = name + ',GPU,hasInterface,' + pcie_version
                file.write(line)
                file.write('\n')
        
        # Storage
        for interface in data['Storage']:
            for storage_type in data['Storage'][interface]:
                for name in data['Storage'][interface][storage_type]:
                    line = name + ',' + storage_type + ',hasInterface,' + interface
                    file.write(line)
                    file.write('\n')
        
        # Main memory
        for interface_version in data['MainMemory']:
            for name in data['MainMemory'][interface_version]:
                line = name + ',' + 'MemoryModuleKit' + ',hasInterface,' + interface_version
                file.write(line)
                file.write('\n')
        

if __name__ == "__main__":
    main()


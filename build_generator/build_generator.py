import json
from random import Random


class Build:
    SEPARATOR = ','
    NAME_PREFIX = 'Build_'
    last_id = 0
    
    def __init__(self, motherboard, cpu, ram_kit, storage_devices = [], gpu = None):
        self.motherboard = motherboard
        self.cpu = cpu
        self.ram_kit = ram_kit
        self.storage_devices = storage_devices
        self.gpu = gpu
        self.id = Build.last_id
        Build.last_id += 1
        
    def to_csv_line(self):
        line = Build.NAME_PREFIX + str(self.id)
        line += Build.SEPARATOR + 'Build' 
        line += Build.SEPARATOR + 'hasMotherboard' + Build.SEPARATOR + self.motherboard
        line += Build.SEPARATOR + 'hasCPU' + Build.SEPARATOR + self.cpu
        line += Build.SEPARATOR + 'hasMemoryKit' + Build.SEPARATOR + self.ram_kit
        if self.gpu is not None:
            line += Build.SEPARATOR + 'hasGPU' + Build.SEPARATOR + self.gpu
        for storage_device in self.storage_devices:
            line += Build.SEPARATOR + 'hasStorage' + Build.SEPARATOR + storage_device
        
        return line

    
class RandomBuild(Build):
    """ 
        Build from random components without compatibility checking.
        This build will have discrete gpu with 0.5 probability
        and <1 , max_num_of_storage> storage devices.
    """
    def __init__(self, data, generator, max_num_of_storage):
        # choose mb
        motherboard = generator.choice(data['Motherboards'])['name']
        # choose cpu
        socket = generator.choice(list(data['CPUs'].keys()))
        all_cpus_of_socket = data['CPUs'][socket]['APU'] + data['CPUs'][socket]['CPU']
        cpu = generator.choice(all_cpus_of_socket)
        # choose ram kit
        slot_version = generator.choice(list(data['MainMemory'].keys()))
        ram_kit = generator.choice(data['MainMemory'][slot_version])
        # choose gpu
        with_gpu = generator.getrandbits(1)
        gpu = None
        if with_gpu:
            pcie_version = generator.choice(list(data['GPUs'].keys()))
            gpu = generator.choice(data['GPUs'][pcie_version])
        # choose storage devices
        storage_devices = []
        number_of_storage_devices = generator.randint(1, max_num_of_storage)
        for i in range(number_of_storage_devices):
            storage_interface = generator.choice(list(data['Storage'].keys()))
            all_storage_devices = data['Storage'][storage_interface]['HDD'] + data['Storage'][storage_interface]['SSD']
            device = generator.choice(all_storage_devices)
            storage_devices.append(device)
        
        super().__init__(motherboard, cpu, ram_kit, storage_devices, gpu)
        
class RandomCompatibleBuild(Build):
    """ Build from random motherboard and compatible components. """
    def __init__(self, data, generator, max_num_of_storage):
        # choose MB
        motherboard = generator.choice(data['Motherboards'])
        mb_interfaces = motherboard['interfaces']
        # find cpu socket on MB
        socket = next(iter(set(mb_interfaces).intersection(set(data['CPUs'].keys()))))
        # check if this MB can be in build with APU
        is_apu_build = False
        if len(data['CPUs'][socket]['APU']) > 0:
            # build is APU build with 0.5 probability or if there are only APU for given socket
            is_apu_build = generator.getrandbits(1) or len(data['CPUs'][socket]['CPU']) == 0
        
        # choose cpu compatible with MB socket
        cpu = None
        if is_apu_build:
            cpu = generator.choice(data['CPUs'][socket]['APU'])
        else:
            cpu = generator.choice(data['CPUs'][socket]['CPU'])
        
        # find ram version of MB
        slot_version = set(motherboard['interfaces']).intersection(set(data['MainMemory'].keys()))
        slot_version = next(iter(slot_version))
        # choose random ram kit compatible with MB
        ram_kit = generator.choice(data['MainMemory'][slot_version])
        
        # decide if build will have discrete GPU
        build_with_discrete_gpu = True
        # APU build can have discrete GPU with 0.5 probability 
        if is_apu_build:
            build_with_discrete_gpu = generator.getrandbits(1)
        
        gpu = None
        if build_with_discrete_gpu:
            # find PCIe version of MB
            pcie_version = set(motherboard['interfaces']).intersection(set(data['GPUs'].keys()))
            pcie_version = next(iter(pcie_version))
            # choose GPU
            gpu = generator.choice(data['GPUs'][pcie_version])
        
        # define list of allowed storage interfaces
        # filter only MB compatible interfaces
        storage_interfaces = set(data['Storage'].keys()).intersection(set(motherboard['interfaces']))
        # workstation build has restricted storage device interfaces
        if socket in data['Metadata']['Workstation']['Sockets']: # workstation build
            ws_storage_interfaces = set(data['Metadata']['Workstation']['StorageInterfaces'])
            storage_interfaces = set(storage_interfaces).intersection(ws_storage_interfaces)
        
        # choose storage devices
        storage_devices = []
        number_of_storage_devices = generator.randint(1, max_num_of_storage)
        
        for i in range(number_of_storage_devices):
            storage_interface = generator.choice(list(storage_interfaces))
            all_storage_devices = data['Storage'][storage_interface]['HDD'] + data['Storage'][storage_interface]['SSD']
            device = generator.choice(all_storage_devices)
            storage_devices.append(device)
        
        
        super().__init__(motherboard['name'], cpu, ram_kit, storage_devices, gpu)
    


def main():
    # load components data
    data = None
    with open('../input_data/all_components.json') as file:
        data = json.load(file)

    # generate some compatible builds and some random build
    generator = Random(2020)
    number_of_compatible_builds = 50
    number_of_random_builds = 50

    builds = []
    for i in range(number_of_compatible_builds):
        builds.append(RandomCompatibleBuild(data, generator, 3))
    
    for i in range(number_of_random_builds):
        builds.append(RandomBuild(data, generator, 3))

    # write list of builds to csv file
    with open('builds.csv', 'w') as file:
        for build in builds:
            file.write(build.to_csv_line())
            file.write('\n')


if __name__ == "__main__":
    main()

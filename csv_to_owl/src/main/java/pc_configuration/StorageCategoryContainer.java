/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import java.util.List;


/**
 *
 * @author Filip Boros
 */
public class StorageCategoryContainer {
    private List<Storage> list;

    public StorageCategoryContainer(List<Storage> list) {
        this.list = list;
    }

    public List<Storage> getList() {
        return list;
    }

    public void setList(List<Storage> list) {
        this.list = list;
    }
    
}

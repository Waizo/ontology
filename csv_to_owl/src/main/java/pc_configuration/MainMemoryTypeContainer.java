/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import java.util.List;

/**
 *
 * @author Filip Boros
 */
public class MainMemoryTypeContainer {
    private List<MainMemory> list;

    public MainMemoryTypeContainer(List<MainMemory> list) {
        this.list = list;
    }

    public List<MainMemory> getList() {
        return list;
    }

    public void setList(List<MainMemory> list) {
        this.list = list;
    }

}

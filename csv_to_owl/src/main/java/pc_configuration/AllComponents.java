/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Filip Boros
 */
public class AllComponents {

    private Map<String, SocketContainer> cpuList;

    private Map<String, GPUTypeContainer> gpuList;

    private Map<String, MainMemoryTypeContainer> mainMemoryList;

    private List<Motherboard> motherboardList;

    private Map<String, StorageTypeContainer> storageList;

    public AllComponents() {
        this.cpuList = new HashMap<>();
        this.gpuList = new HashMap<>();
        this.mainMemoryList = new HashMap<>();
        this.motherboardList = new ArrayList<>();
        this.storageList = new HashMap<>();
    }

    public Map<String, SocketContainer> getCpuList() {
        return cpuList;
    }

    public void setCpuList(Map<String, SocketContainer> cpuList) {
        this.cpuList = cpuList;
    }

    public Map<String, GPUTypeContainer> getGpuList() {
        return gpuList;
    }

    public void setGpuList(Map<String, GPUTypeContainer> gpuList) {
        this.gpuList = gpuList;
    }

    public Map<String, MainMemoryTypeContainer> getMainMemoryList() {
        return mainMemoryList;
    }

    public void setMainMemoryList(Map<String, MainMemoryTypeContainer> mainMemoryList) {
        this.mainMemoryList = mainMemoryList;
    }

    public List<Motherboard> getMotherboardList() {
        return motherboardList;
    }

    public void setMotherboardList(List<Motherboard> motherboardList) {
        this.motherboardList = motherboardList;
    }

    public Map<String, StorageTypeContainer> getStorageList() {
        return storageList;
    }

    public void setStorageList(Map<String, StorageTypeContainer> storageList) {
        this.storageList = storageList;
    }

    public List<CPU> filterCPU(String socket, String type) {
        return this.cpuList.get(socket).getList().get(type).getList();
    }

    public List<Storage> filterStorages(String type, String category) {
        return this.storageList.get(type).getList().get(category).getList();
    }

    public List<Motherboard> filterMotherboards(Set<String> interfaces) {
        List<Motherboard> filtered = new ArrayList<>();
        this.motherboardList.stream().filter((mb) -> (mb.containsAllInterfaces(interfaces))).forEachOrdered((mb) -> {
            filtered.add(mb);
        });
        return filtered;
    }

    public List<GPU> filterGPU(String type) {
        return this.gpuList.get(type).getList();
    }

    public List<MainMemory> filterRam(String type) {
        return this.mainMemoryList.get(type).getList();
    }

}

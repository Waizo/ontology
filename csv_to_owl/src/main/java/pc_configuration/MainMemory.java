/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

/**
 *
 * @author Filip Boros
 */
public class MainMemory extends Component{
    private String type;

    public MainMemory() {
        this.type = "";
    }

    public MainMemory(String type, String name) {
        super(name);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import java.util.List;

/**
 *
 * @author Filip Boros
 */
public class GPUTypeContainer {
    private List<GPU> list;

    public GPUTypeContainer(List<GPU> list) {
        this.list = list;
    }

    public List<GPU> getList() {
        return list;
    }

    public void setList(List<GPU> list) {
        this.list = list;
    }
    
}

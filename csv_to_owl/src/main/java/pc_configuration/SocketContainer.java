/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Filip Boros
 */
public class SocketContainer {
    private Map<String, CPUTypeContainer> list;

    public SocketContainer() {
        this.list = new HashMap<>();
    }

    public Map<String, CPUTypeContainer> getList() {
        return list;
    }

    public void setList(Map<String, CPUTypeContainer> list) {
        this.list = list;
    }
    
}

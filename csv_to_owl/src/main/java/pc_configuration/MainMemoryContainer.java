/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Filip Boros
 */
class MainMemoryContainer {
    private Map<String, MainMemoryTypeContainer> list;

    public MainMemoryContainer() {
        this.list = new HashMap<>();
    }

    public Map<String, MainMemoryTypeContainer> getList() {
        return list;
    }

    public void setList(Map<String, MainMemoryTypeContainer> list) {
        this.list = list;
    }
    
}

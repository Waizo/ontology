/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Filip Boros
 */
public class GPUContainer {
    private Map<String, GPUTypeContainer> list;

    public GPUContainer() {
        this.list = new HashMap<>();
    }

    public Map<String, GPUTypeContainer> getList() {
        return list;
    }

    public void setList(Map<String, GPUTypeContainer> list) {
        this.list = list;
    }
    
}

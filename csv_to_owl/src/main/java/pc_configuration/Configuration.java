/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Filip Boros
 */
public class Configuration {
    private String name;
    private CPU cpu;
    private GPU gpu;
    private Motherboard motherboard;
    private MainMemory mainMemory;
    private List<Storage> storages;

    public Configuration() {
        this.name = "";
        this.cpu = null;
        this.gpu = null;
        this.motherboard = null;
        this.mainMemory = null;
        this.storages = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public CPU getCpu() {
        return cpu;
    }

    public void setCpu(CPU cpu) {
        this.cpu = cpu;
    }

    public GPU getGpu() {
        return gpu;
    }

    public void setGpu(GPU gpu) {
        this.gpu = gpu;
    }

    public Motherboard getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(Motherboard motherboard) {
        this.motherboard = motherboard;
    }

    public MainMemory getMainMemory() {
        return mainMemory;
    }

    public void setMainMemory(MainMemory mainMemory) {
        this.mainMemory = mainMemory;
    }

    public List<Storage> getStorages() {
        return storages;
    }

    public void setStorages(List<Storage> storages) {
        this.storages = storages;
    }  
    
}

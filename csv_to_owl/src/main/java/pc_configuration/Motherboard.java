/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import java.util.Set;

/**
 *
 * @author Filip Boros
 */
public class Motherboard extends Component {

    private Interfaces interfaces;

    public Motherboard() {
        this.interfaces = new Interfaces();
    }

        public Motherboard(String name) {
        super(name);
    }
    
    public Interfaces getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(Interfaces interfaces) {
        this.interfaces = interfaces;
    }

    public boolean containsAllInterfaces(Set<String> interfacesFilter) {
        return interfacesFilter.stream().noneMatch((intf) -> (!interfaces.getInterfaces().stream().anyMatch((interf) -> (interf.equals(intf)))));
    }

    private boolean containsInterface(String intf) {
        return interfaces.getInterfaces().stream().anyMatch((interf) -> (interf.equals(intf)));
    }

}

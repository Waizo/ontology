/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

/**
 *
 * @author Filip Boros
 */
public class CPU extends Component{
    private String socket;
    private String type;

    public CPU() {
        this.socket = "";
        this.type = null;      
    }

    public CPU(String socket, String type, String name) {
        super(name);
        this.socket = socket;
        this.type = type;
    }

    public String getSocket() {
        return socket;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pc_configuration;

import java.util.List;

/**
 *
 * @author Filip Boros
 */
public class CPUTypeContainer {
    private List<CPU> list;

    public CPUTypeContainer(List<CPU> list) {
        this.list = list;
    }

    public List<CPU> getList() {
        return list;
    }

    public void setList(List<CPU> list) {
        this.list = list;
    }
    
    
}

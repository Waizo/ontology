/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csvDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Filip Boros
 */
public class Record {

    private String individualityName;
    private String individualityClass;
    private List<Property> properties;

    public Record() {
        this.individualityName = "";
        this.individualityClass = "";
        this.properties = new ArrayList<>();
    }

    public Record(String individualityName, String individualityClass, List<Property> properties) {
        this.individualityName = individualityName;
        this.individualityClass = individualityClass;
        this.properties = properties;
    }

    public String getIndividualityName() {
        return individualityName;
    }

    public void setIndividualityName(String individualityName) {
        this.individualityName = individualityName;
    }

    public String getIndividualityClass() {
        return individualityClass;
    }

    public void setIndividualityClass(String individualityClass) {
        this.individualityClass = individualityClass;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }
      
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileHandlers;

import com.univocity.parsers.common.DataProcessingException;
import com.univocity.parsers.common.ParsingContext;
import com.univocity.parsers.common.RowProcessorErrorHandler;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Filip Boros
 */
public class OntologyRowProcessorErrorHandler implements RowProcessorErrorHandler {

    @Override
    public void handleError(DataProcessingException dpe, Object[] os, ParsingContext t) {
        JOptionPane.showMessageDialog(new JFrame(),"Error in processing rows from source file","Error",JOptionPane.ERROR_MESSAGE);
        System.out.println("Error processing row: "
                + Arrays.toString(os) + "\n" + "Error details: column '"
                + dpe.getColumnName() + "' (index " + dpe.getColumnIndex()
                + ") has value '" + os[dpe.getColumnIndex()] + "'");
    }

}

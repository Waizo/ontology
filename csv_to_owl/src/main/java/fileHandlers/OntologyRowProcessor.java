/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileHandlers;

import com.univocity.parsers.common.ParsingContext;
import com.univocity.parsers.common.processor.RowProcessor;
import csvDataModel.Property;
import csvDataModel.Record;
import java.util.ArrayList;
import core.OntologyParser;

/**
 *
 * @author Filip Boros
 */
public class OntologyRowProcessor implements RowProcessor {

    private OntologyParser ontology;
    private String fileName;

    public OntologyRowProcessor(OntologyParser ontology, String fileName) {
        this.ontology = ontology;
        this.fileName = fileName;
    }

    @Override
    public void processStarted(ParsingContext pc) {
        ontology.setIndividualities(new ArrayList<>());
        System.out.println("Started loading file: " + fileName);
    }

    @Override
    public void rowProcessed(String[] strings, ParsingContext pc) {
        int noEmptyStringsCount = 0;
        for (String str : strings) {
            if (!str.isEmpty()) {
                noEmptyStringsCount++;
            }
        }
        if (strings[0].isEmpty() || (noEmptyStringsCount > 1 && strings[1].isEmpty()) 
                || (noEmptyStringsCount % 2 != 0 && noEmptyStringsCount != 1)) {
            return;
        }

        Record record = new Record();
        record.setIndividualityName(strings[0]);
        record.setIndividualityClass(strings[1]);
        int counter = 2;
        while ((counter + 1) < strings.length) {
            if (!strings[counter].isEmpty() && !strings[counter+1].isEmpty()) {
                Property property = new Property();
                property.setPropertyName(strings[counter]);
                counter++;
                property.setPropertyValue(strings[counter]);
                counter++;
                record.getProperties().add(property);
            } else {
                counter++;
            }
        }
        ontology.getIndividualities().add(record);
    }

    @Override
    public void processEnded(ParsingContext pc) {
        System.out.println("Ended loading file: " + fileName);
    }

}

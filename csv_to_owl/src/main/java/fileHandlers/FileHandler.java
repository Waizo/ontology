/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileHandlers;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import core.OntologyParser;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import pc_configuration.Configuration;
import pc_configuration.Storage;

/**
 *
 * @author Filip Boros
 */
public class FileHandler {

    private OntologyParser ontology;
    private JAXBContext jaxbContext;
    private Marshaller jaxbMarshaller;

    public FileHandler(OntologyParser ontology) {
        this.ontology = ontology;
        try {
            this.jaxbContext = JAXBContext.newInstance(OntologyParser.class);
            this.jaxbMarshaller = jaxbContext.createMarshaller();
            this.jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (JAXBException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(new JFrame(), "Error in initializing XML handlers", "Error", JOptionPane.ERROR_MESSAGE);
            System.err.println("Error in initializing XML handlers. " + ex.getMessage());
        }
    }

    public FileHandler() {
    }

    public void readCsvFile(String fileName) {
        CsvParserSettings settings = new CsvParserSettings();
        //settings.getFormat().setLineSeparator("\n");
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setNullValue("");
        settings.setEmptyValue("");
        settings.setProcessor(new OntologyRowProcessor(ontology, fileName));
        settings.setProcessorErrorHandler(new OntologyRowProcessorErrorHandler());

        CsvParser parser = new CsvParser(settings);
        parser.parse(new File(fileName));
    }

    public void storeXML(String fileName) {
        try {
            jaxbMarshaller.marshal(ontology, new File(fileName));
        } catch (JAXBException ex) {
            JOptionPane.showMessageDialog(new JFrame(), "Error while storing XML file", "Error", JOptionPane.ERROR_MESSAGE);
            System.err.println("Error while storing XML file. " + ex.getMessage());
        }
    }

    private static void removeEmptyText(Node node) {
        Node child = node.getFirstChild();
        while (child != null) {
            Node sibling = child.getNextSibling();
            if (child.getNodeType() == Node.TEXT_NODE) {
                if (child.getTextContent().trim().isEmpty()) {
                    node.removeChild(child);
                }
            } else {
                removeEmptyText(child);
            }
            child = sibling;
        }
    }

    public void addBuildToOntology(String fileName, Configuration build) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(fileName);

            Element root = document.getDocumentElement();

            Element declaration = document.createElement("Declaration");
            //declaration.appendChild(document.createTextNode("test")); //hodnota do tagu
            Element declaration_child1 = document.createElement("NamedIndividual");
            declaration_child1.setAttribute("IRI", "#" + build.getName());
            declaration.appendChild(declaration_child1); //hodnota do tagu

            root.appendChild(declaration);

            Element classAssertion1 = document.createElement("ClassAssertion");
            Element classAssertion1_child1 = document.createElement("Class");
            classAssertion1_child1.setAttribute("IRI", "#Build");
            classAssertion1.appendChild(classAssertion1_child1);
            Element classAssertion1_child2 = document.createElement("NamedIndividual");
            classAssertion1_child2.setAttribute("IRI", "#" + build.getName());
            classAssertion1.appendChild(classAssertion1_child2);

            root.appendChild(classAssertion1);

            //Motherboard
            Element objectPropertyAssertion = document.createElement("ObjectPropertyAssertion");
            Element objectPropertyAssertion_child1 = document.createElement("ObjectProperty");
            objectPropertyAssertion_child1.setAttribute("IRI", "#hasMotherboard");
            objectPropertyAssertion.appendChild(objectPropertyAssertion_child1);

            Element objectPropertyAssertion_child2 = document.createElement("NamedIndividual");
            objectPropertyAssertion_child2.setAttribute("IRI", "#" + build.getName());
            objectPropertyAssertion.appendChild(objectPropertyAssertion_child2);

            Element objectPropertyAssertion_child3 = document.createElement("NamedIndividual");
            objectPropertyAssertion_child3.setAttribute("IRI", "#" + build.getMotherboard());
            objectPropertyAssertion.appendChild(objectPropertyAssertion_child3);

            root.appendChild(objectPropertyAssertion);

            //CPU
            Element objectPropertyAssertion2 = document.createElement("ObjectPropertyAssertion");
            Element objectPropertyAssertion2_child1 = document.createElement("ObjectProperty");
            objectPropertyAssertion2_child1.setAttribute("IRI", "#hasCPU");
            objectPropertyAssertion2.appendChild(objectPropertyAssertion2_child1);

            Element objectPropertyAssertion2_child2 = document.createElement("NamedIndividual");
            objectPropertyAssertion2_child2.setAttribute("IRI", "#" + build.getName());
            objectPropertyAssertion2.appendChild(objectPropertyAssertion2_child2);

            Element objectPropertyAssertion2_child3 = document.createElement("NamedIndividual");
            objectPropertyAssertion2_child3.setAttribute("IRI", "#" + build.getCpu());
            objectPropertyAssertion2.appendChild(objectPropertyAssertion2_child3);

            root.appendChild(objectPropertyAssertion2);

            //CPU
            Element objectPropertyAssertion3 = document.createElement("ObjectPropertyAssertion");
            Element objectPropertyAssertion3_child1 = document.createElement("ObjectProperty");
            objectPropertyAssertion3_child1.setAttribute("IRI", "#hasMemoryKit");
            objectPropertyAssertion3.appendChild(objectPropertyAssertion3_child1);

            Element objectPropertyAssertion3_child2 = document.createElement("NamedIndividual");
            objectPropertyAssertion3_child2.setAttribute("IRI", "#" + build.getName());
            objectPropertyAssertion3.appendChild(objectPropertyAssertion3_child2);

            Element objectPropertyAssertion3_child3 = document.createElement("NamedIndividual");
            objectPropertyAssertion3_child3.setAttribute("IRI", "#" + build.getMainMemory());
            objectPropertyAssertion3.appendChild(objectPropertyAssertion3_child3);

            root.appendChild(objectPropertyAssertion3);

            //GPU
            if (build.getGpu() != null) {
                Element objectPropertyAssertion1 = document.createElement("ObjectPropertyAssertion");
                Element objectPropertyAssertion1_child1 = document.createElement("ObjectProperty");
                objectPropertyAssertion1_child1.setAttribute("IRI", "#hasGPU");
                objectPropertyAssertion1.appendChild(objectPropertyAssertion1_child1);

                Element objectPropertyAssertion1_child2 = document.createElement("NamedIndividual");
                objectPropertyAssertion1_child2.setAttribute("IRI", "#" + build.getName());
                objectPropertyAssertion1.appendChild(objectPropertyAssertion1_child2);

                Element objectPropertyAssertion1_child3 = document.createElement("NamedIndividual");
                objectPropertyAssertion1_child3.setAttribute("IRI", "#" + build.getGpu());
                objectPropertyAssertion1.appendChild(objectPropertyAssertion1_child3);

                root.appendChild(objectPropertyAssertion1);
            }

            for (Storage storage : build.getStorages()) {
                Element objectPropertyAssertion4 = document.createElement("ObjectPropertyAssertion");
                Element objectPropertyAssertion4_child1 = document.createElement("ObjectProperty");
                objectPropertyAssertion4_child1.setAttribute("IRI", "#hasStorage");
                objectPropertyAssertion4.appendChild(objectPropertyAssertion4_child1);

                Element objectPropertyAssertion4_child2 = document.createElement("NamedIndividual");
                objectPropertyAssertion4_child2.setAttribute("IRI", "#" + build.getName());
                objectPropertyAssertion4.appendChild(objectPropertyAssertion4_child2);

                Element objectPropertyAssertion4_child3 = document.createElement("NamedIndividual");
                objectPropertyAssertion4_child3.setAttribute("IRI", "#" + storage.getName());
                objectPropertyAssertion4.appendChild(objectPropertyAssertion4_child3);

                root.appendChild(objectPropertyAssertion4);
            }

            DOMSource source = new DOMSource(document);
            removeEmptyText(document.getDocumentElement());

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            StreamResult result = new StreamResult(fileName);
            transformer.transform(source, result);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException ex) {
            Logger.getLogger(FileHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}

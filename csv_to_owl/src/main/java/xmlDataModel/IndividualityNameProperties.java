/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlDataModel;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Filip Boros
 */
public class IndividualityNameProperties {
    
    @XmlElement(name = "ObjectProperty")
    private StringNode propertyName;
    
    @XmlElement(name = "NamedIndividual")
    private StringNode individualityName;
    
    @XmlElement(name = "NamedIndividual")
    private StringNode propertyValue;

    public IndividualityNameProperties(String individualityName, String propertyName, String propertyValue) {
        this.individualityName = new StringNode(individualityName);
        this.propertyName = new StringNode(propertyName);
        this.propertyValue = new StringNode(propertyValue);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlDataModel;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Filip Boros
 */
public class IndividualityNameClass {
    
    @XmlElement(name = "Class")
    private StringNode individualityClass;
    
    @XmlElement(name = "NamedIndividual")
    private StringNode individualityName;
    
    public IndividualityNameClass(String individualityName, String individualityClass) {
        this.individualityName = new StringNode(individualityName);
        this.individualityClass = new StringNode(individualityClass);
    }

}

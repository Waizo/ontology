/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlDataModel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Filip Boros
 */
public class IndividualityName {
    
    @XmlElement(name ="NamedIndividual")
    private StringNode individualityName;

    public IndividualityName(String individualityName) {
        this.individualityName = new StringNode(individualityName);
    }

}

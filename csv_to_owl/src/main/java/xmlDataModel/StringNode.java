/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlDataModel;

import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Filip Boros
 */
public class StringNode {
    
    @XmlAttribute(name ="IRI")
    private String name;

    public StringNode(String name) {
        this.name = name;
    }
}

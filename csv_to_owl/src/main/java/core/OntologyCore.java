/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;
import fileHandlers.FileHandler;
import gui.OntologySourceDialog;
import java.awt.Frame;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import pc_configuration.AllComponents;
import pc_configuration.Configuration;

/**
 *
 * @author Filip Boros
 */
public class OntologyCore implements Serializable {

    transient private OntologyParser ontologyParser;
    transient private ComponentsParser componentsParser;
    transient private AllComponents componentsList;

    transient private Configuration configuration;

    private int buildCounter;
    private String componentsSourcePath;

    public OntologyCore() {
        this.ontologyParser = new OntologyParser();
        this.componentsParser = new ComponentsParser();
        this.componentsList = null;
        this.configuration = new Configuration();
        if (componentsSourcePath == null || componentsSourcePath.isEmpty()) {

        }
        initializeComponentsList();
        loadBuildCounter();
    }

    private void initializeComponentsList() {
        try {
            JSONObject obj = (JSONObject) new JSONParser().parse(new FileReader("all_components.json"));
            ComponentsParser parser = new ComponentsParser();
            parser.fillComponents(obj);
            componentsList = parser.getComponentsContainer();
        } catch (IOException | ParseException ex) {
            JOptionPane.showMessageDialog(new JFrame(), "Could no load file with defined components at: " + componentsSourcePath, "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
            this.componentsList = null;
        }
    }

    public OntologyParser getOntologyParser() {
        return ontologyParser;
    }

    public ComponentsParser getComponentsParser() {
        return componentsParser;
    }

    public AllComponents getComponentsList() {
        return componentsList;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public int getBuildCounter() {
        return buildCounter;
    }

    public void setBuildCounter(int buildCounter) {
        this.buildCounter = buildCounter;
    }

    public void storeBuildCounter() {
        FileOutputStream f;
        try {
            f = new FileOutputStream(new File("data.dat"));
            try (ObjectOutputStream o = new ObjectOutputStream(f)) {
                o.writeObject(buildCounter);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OntologyCore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OntologyCore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadBuildCounter() {
        FileInputStream fi;
        try {
            fi = new FileInputStream(new File("data.dat"));
            // Read objects
            try (ObjectInputStream oi = new ObjectInputStream(fi)) {
                // Read objects
                this.buildCounter = (int) oi.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(new JFrame(), "Could not load build counter value, it's value was set to 10 000. Check for missing file data.dat.", "Warning", JOptionPane.WARNING_MESSAGE);
            this.buildCounter = 10000;
        }
    }

    public void storeBuildIntoOntology() {
        OntologySourceDialog dialog = new OntologySourceDialog(new Frame(), true);
        dialog.setVisible(true);
        String ontologyPath = dialog.getInputFileName();
        if (!ontologyPath.equals("")) {
            buildCounter++;
            storeBuildCounter();
            this.configuration.setName("".equals(this.configuration.getName()) ? "Build_" + buildCounter : this.configuration.getName() + "_" + buildCounter);
            FileHandler fh = new FileHandler();
            fh.addBuildToOntology(ontologyPath, this.configuration);
            JOptionPane.showMessageDialog(new JFrame(), "Build stored in " + ontologyPath, "Export successful", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void storeBuildAsCSV() {
        CsvWriterSettings settings = new CsvWriterSettings();
        // Sets the character sequence to write for the values that are null.
        settings.setNullValue("");

        //Changes the comment character to -
        settings.getFormat().setComment('-');

        // Sets the character sequence to write for the values that are empty.
        settings.setEmptyValue("");

        // writes empty lines as well.
        settings.setSkipEmptyLines(false);

        // Creates a writer with the above settings;
        CsvWriter writer = null;
        try {
            writer = new CsvWriter(new BufferedWriter(new FileWriter(new File("builds.csv"), false)), settings);
        } catch (IOException ex) {
            return;
        }

        writer.addValue(configuration.getName() + "_" + buildCounter++);
        storeBuildCounter();
        writer.addValue("Build");
        writer.addValue("hasMotherboard");
        writer.addValue(configuration.getMotherboard());
        writer.addValue("hasCPU");
        writer.addValue(configuration.getCpu());
        writer.addValue("hasMemory");
        writer.addValue(configuration.getMainMemory());

        if (configuration.getGpu() != null) {
            writer.addValue("hasGPU");
            writer.addValue(configuration.getGpu());
        } else {
            writer.addValue("");
            writer.addValue("");
        }

        for (int i = 0; i < configuration.getStorages().size(); i++) {
            writer.addValue("hasStorage");
            writer.addValue(configuration.getStorages().get(i).getName());
        }
        writer.writeValuesToRow();
        writer.close();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.Arrays;
import java.util.Map;

/**
 *
 * @author Filip Boros
 */
public class Utilities {

    public static String[] convertObjArrToStr(Object[] original) {
        return Arrays.copyOf(original, original.length, String[].class);
    }
}

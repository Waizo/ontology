/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import csvDataModel.Property;
import csvDataModel.Record;
import de.uni_stuttgart.vis.vowl.owl2vowl.Owl2Vowl;
import fileHandlers.FileHandler;
import gui.FileNamesDialog;
import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.semanticweb.owlapi.model.IRI;
import xmlDataModel.IndividualityName;
import xmlDataModel.IndividualityNameClass;
import xmlDataModel.IndividualityNameProperties;

/**
 *
 * @author Filip Boros
 */
@XmlRootElement(name = "OntologyParser")
@XmlAccessorType(XmlAccessType.FIELD)
public class OntologyParser {

    @XmlTransient
    private List<Record> individualities;

    @XmlElement(name = "Declaration")
    private List<IndividualityName> individualityNames;

    @XmlElement(name = "ClassAssertion")
    private List<IndividualityNameClass> individualityNameClass;

    @XmlElement(name = "ObjectPropertyAssertion")
    private List<IndividualityNameProperties> individualityNameProperties;

    public OntologyParser() {
        this.individualities = new ArrayList<>();
        this.individualityNames = new ArrayList<>();
        this.individualityNameClass = new ArrayList<>();
        this.individualityNameProperties = new ArrayList<>();
    }

    public OntologyParser(List<Record> individualities) {
        this.individualities = individualities;
    }

    public List<Record> getIndividualities() {
        return individualities;
    }

    public void setIndividualities(List<Record> individualities) {
        this.individualities = individualities;
    }

    public void fillListsForParsing() {
        individualities.stream().map((individuality) -> {
            if (!individuality.getIndividualityName().isEmpty()) {
                individualityNames.add(new IndividualityName("#" + individuality.getIndividualityName()));
            }
            return individuality;
        }).map((individuality) -> {
            if (!individuality.getIndividualityClass().isEmpty() && !individuality.getIndividualityName().isEmpty()) {
                individualityNameClass.add(new IndividualityNameClass("#" + individuality.getIndividualityName(), "#" + individuality.getIndividualityClass()));
            }
            return individuality;
        }).forEachOrdered((individuality) -> {
            individuality.getProperties().stream().filter((property) -> (!property.getPropertyName().isEmpty() && !property.getPropertyValue().isEmpty() && !individuality.getIndividualityName().isEmpty())).forEachOrdered((property) -> {
                individualityNameProperties.add(new IndividualityNameProperties("#" + individuality.getIndividualityName(),
                        "#" + property.getPropertyName(), "#" + property.getPropertyValue()));
            });
        });
    }

    public void parseOWL2VOWL(String sourceFilePath, String targetFilePath) {
        if (sourceFilePath.isEmpty() || targetFilePath.isEmpty()) {
            JOptionPane.showMessageDialog(new JFrame(), "Prázdne cesty k súborom", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            Owl2Vowl owlParser = new Owl2Vowl(IRI.create(new File(sourceFilePath)));
            //owlParser.getJsonAsString();
            owlParser.writeToFile(new File(targetFilePath));
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(new JFrame(), "Prázdne cesty k súborom", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void parseCSV2XML(String sourceFilePath, String targetFilePath) {
        if (sourceFilePath.isEmpty() || targetFilePath.isEmpty()) {
            JOptionPane.showMessageDialog(new JFrame(), "Prázdne cesty k súborom", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        FileHandler fh = new FileHandler(this);

        fh.readCsvFile(sourceFilePath);

        this.fillListsForParsing();

        fh.storeXML(targetFilePath);

        JOptionPane.showMessageDialog(new JFrame(), "XML súbor bol úspešne vytvorený", "Hotovo", JOptionPane.INFORMATION_MESSAGE);

        return;
    }

}

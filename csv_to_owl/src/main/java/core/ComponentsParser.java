/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import pc_configuration.AllComponents;
import pc_configuration.CPU;
import pc_configuration.GPU;
import pc_configuration.CPUContainer;
import pc_configuration.CPUTypeContainer;
import pc_configuration.GPUTypeContainer;
import pc_configuration.Interfaces;
import pc_configuration.MainMemory;
import pc_configuration.MainMemoryTypeContainer;
import pc_configuration.Motherboard;
import pc_configuration.SocketContainer;
import pc_configuration.Storage;
import pc_configuration.StorageCategoryContainer;
import pc_configuration.StorageTypeContainer;

/**
 *
 * @author Filip Boros
 */
public class ComponentsParser {

    private AllComponents componentsContainer;
    private Set<String> allInterfaces;
    private JSONObject source;

    public ComponentsParser() {
        this.componentsContainer = new AllComponents();
        this.allInterfaces = new HashSet<>(); 
        this.source = null;
    }

    public void fillComponents(JSONObject source) {
        this.source = source;
        fillCPUs();
        fillGPUs();
        fillMainMemories();
        fillMotherBoards();
        fillStorages();
    }

    public AllComponents getComponentsContainer() {
        return componentsContainer;
    }

    public Set<String> getAllInterfaces() {
        return allInterfaces;
    }
    
    private void fillCPUs() {
        Map<String, SocketContainer> cpuList = new HashMap<>();

        Iterator<Map.Entry> itr = ((Map) source.get("CPUs")).entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry pair = itr.next();
            SocketContainer socketContainer = new SocketContainer();

            Iterator<Map.Entry> itr2 = ((Map) pair.getValue()).entrySet().iterator();
            while (itr2.hasNext()) {
                Map.Entry pair2 = itr2.next();
                CPUTypeContainer cpuTypeContainer = new CPUTypeContainer(new ArrayList<>());

                Iterator itr3 = ((JSONArray) pair2.getValue()).iterator();
                while (itr3.hasNext()) {
                    CPU cpu = new CPU((String) pair.getKey(), (String) pair2.getKey(), (String) itr3.next());
                    cpuTypeContainer.getList().add(cpu);
                }

                socketContainer.getList().put(String.valueOf(pair2.getKey()), cpuTypeContainer);
            }

            cpuList.put(String.valueOf(pair.getKey()), socketContainer);
        }
        componentsContainer.setCpuList(cpuList);
    }

    private void fillGPUs() {
        Map<String, GPUTypeContainer> gpuList = new HashMap<>();

        Iterator<Map.Entry> itr = ((Map) source.get("GPUs")).entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry pair = itr.next();
            GPUTypeContainer gpuTypeContainer = new GPUTypeContainer(new ArrayList<>());

            Iterator itr2 = ((JSONArray) pair.getValue()).iterator();
            while (itr2.hasNext()) {
                GPU gpu = new GPU((String) pair.getKey(), (String) itr2.next());
                gpuTypeContainer.getList().add(gpu);
            }

            gpuList.put(String.valueOf(pair.getKey()), gpuTypeContainer);
        }
        componentsContainer.setGpuList(gpuList);
    }

    private void fillMainMemories() {
        Map<String, MainMemoryTypeContainer> mainMemoryList = new HashMap<>();

        Iterator<Map.Entry> itr = ((Map) source.get("MainMemory")).entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry pair = itr.next();
            MainMemoryTypeContainer mainMemoryTypeContainer = new MainMemoryTypeContainer(new ArrayList<>());

            Iterator itr2 = ((JSONArray) pair.getValue()).iterator();
            while (itr2.hasNext()) {
                MainMemory mainMemory = new MainMemory((String) pair.getKey(), (String) itr2.next());
                mainMemoryTypeContainer.getList().add(mainMemory);
            }

            mainMemoryList.put(String.valueOf(pair.getKey()), mainMemoryTypeContainer);
        }
        componentsContainer.setMainMemoryList(mainMemoryList);
    }

    private void fillMotherBoards() {
        List<Motherboard> motherboardList = new ArrayList<>();

        Iterator itr = ((JSONArray) source.get("Motherboards")).iterator();
        while (itr.hasNext()) {
            JSONObject obj = (JSONObject) itr.next();
            Motherboard motherboard = new Motherboard();
            motherboard.setName((String) obj.get("name"));
            Interfaces interfaces = new Interfaces();

            Iterator itr2 = ((JSONArray) (((Map) obj).get("interfaces"))).iterator();
            while (itr2.hasNext()) {
                String interfaceName = (String) itr2.next();
                interfaces.getInterfaces().add(interfaceName);
                if (!allInterfaces.contains(interfaceName)) {
                    allInterfaces.add(interfaceName);
                }
            }
            motherboard.setInterfaces(interfaces);
            motherboardList.add(motherboard);
            //MainMemory mainMemory = new MainMemory((String) pair.getKey(), (String) itr2.next());
            //mainMemoryTypeContainer.getList().add(mainMemory);
        }
        componentsContainer.setMotherboardList(motherboardList);
    }

    private void fillStorages() {
        Map<String, StorageTypeContainer> storageList = new HashMap<>();

        Iterator<Map.Entry> itr = ((Map) source.get("Storage")).entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry pair = itr.next();
            StorageTypeContainer storageTypeContainer = new StorageTypeContainer();

            Iterator<Map.Entry> itr2 = ((Map) pair.getValue()).entrySet().iterator();
            while (itr2.hasNext()) {
                Map.Entry pair2 = itr2.next();
                StorageCategoryContainer storageCategoryContainer = new StorageCategoryContainer(new ArrayList<>());

                Iterator itr3 = ((JSONArray) pair2.getValue()).iterator();
                while (itr3.hasNext()) {
                    Storage storage = new Storage((String) pair.getKey(), (String) pair2.getKey(), (String) itr3.next());
                    storageCategoryContainer.getList().add(storage);
                }

                storageTypeContainer.getList().put(String.valueOf(pair2.getKey()), storageCategoryContainer);
            }

            storageList.put(String.valueOf(pair.getKey()), storageTypeContainer);
        }
        componentsContainer.setStorageList(storageList);
    }
}
